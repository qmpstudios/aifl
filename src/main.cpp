#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Joy.h>
#include <cstdlib>
#include <ctime>
#include "sensor_msgs/Range.h"
#include <sensor_msgs/PointCloud.h>
#include <tf/LinearMath/Quaternion.h>


using namespace std;



double x_3 = 0.0;
double x_2 = 0.0;
double x_2_stop = 0.0;
double y_5_start = 0.0;
double y_5_current = 0.0;

double angle_start = 0.0;
double angle_current = 0.0;
enum EVENT {Dojazd, ObrotLewo, JedzWzdluz, ObrotPrawo};

void setFuzzyValues()
{

}


// ----------------- callbacks ----------------- 

void sonarCallback(const sensor_msgs::PointCloud& msg)
{

double fr_lx, fr_ly;


//fr_lx = msg.points[3].x;
//fr_ly = msg.points[3].y;

x_3 = msg.points[3].x;
x_2 = msg.points[2].x;
x_3 = msg.points[3].x;
x_2 = msg.points[2].x;
y_5_current = msg.points[5].y;

  //ROS_INFO("Sonar[3] x: %f, y: %f", fr_lx, fr_ly);
}

void poseCallback(const nav_msgs::Odometry &msg)
{
    tf::Quaternion quaternion(0,0,0,0);
    quaternion.setW(msg.pose.pose.orientation.w);
    quaternion.setZ(msg.pose.pose.orientation.z);

    tfScalar scalar = quaternion.getAngle();
    //ROS_INFO("ANGLE: %f", scalar);
    angle_current = scalar;

}
//sensor_msgs::PointCloud& msg)}



// ----------------- Twoje funkcje ----------------- 

// wejście
// fuzzyfikacja
// reguły
// defuzzyfikacja
// reakcja (wyjście)

void drive()
{

}



// ----------------- funkcja główna ----------------- 
int main(int argc, char** argv)
{
    srand(time(0));
    geometry_msgs::Twist cmdvelmsg; // zmienna prędkości robota
    cmdvelmsg.linear.x  = 0.0;
    cmdvelmsg.angular.z = 0;
    bool drive = true;	// zmienna trybu

    ros::init(argc, argv, "aifl1");
    ROS_INFO("NODE STARTED");

    //Ros variables
    ros::NodeHandle n;
    ros::Rate rate(30);

    ros::Subscriber sonarsub = n.subscribe("/amigobot/sonar", 1, sonarCallback); // ROS reaguje na dane z sensora
    ros::Publisher cmdvelpub = n.advertise<geometry_msgs::Twist>("/amigobot/cmd_vel", 1000);   // ROS wysyła prędkości do robota
    ros::Subscriber odompub = n.subscribe("/amigobot/pose", 1, poseCallback); //Odometry subscriber

    //double fuzzy_table[20];
    int i = 0;

    EVENT event = Dojazd;
    double near = 0.0;
    double far = 0.0;



// ----------------- pętla główna ----------------- 
    while(n.ok())
    {
        //Sleep
        rate.sleep();
        ros::spinOnce();
        drive = true;



        switch(event){
            case Dojazd:
            {

                ///set the fuzzy values
                ///near value
                if(x_3 <  0.2 || x_2 < 0.2)
                {
                    near = 5;
                }

                else if(x_3 < 0.4 || x_2 < 0.4)
                {
                    near = 3;
                }

                else if(x_3 < 0.8 || x_2 < 0.8)
                {
                    near = 1;
                }

                else
                    near = 0;

                ///far value
                if(x_3 > 0.8 || x_2 > 0.8)
                {
                    far = 5;
                }

                else if(x_3 > 0.6 || x_2 > 0.6)
                {
                    far = 3;
                }

                else if(x_3 > 0.4 || x_2 > 0.4)
                {
                    far = 1;
                }

                else
                    far = 0;

                double fuzzy = far - near;
                double base_velocity = 0.1;


                cmdvelmsg.linear.x = base_velocity * fuzzy;
                cmdvelpub.publish(cmdvelmsg);

               // ROS_INFO("FUZZY: %f", fuzzy);


                if(fuzzy == 0)
                {
                    ++i;
                    //ROS_INFO("FUZZY 0 ITERATOR: %d", i);
                }

                if(i>50)
                {
                    event = ObrotLewo;
                    angle_start = angle_current;
                    x_2_stop = x_2;
                }

                break;
            }
            case ObrotLewo:
            {


                if(angle_current-angle_start<1.57)
                {
                    cmdvelmsg.angular.z = 0.2;
                    //ROS_INFO("Hello: krece sie");
                }
                else
                {
                    cmdvelmsg.angular.z = 0.0;
                    event = JedzWzdluz;
                    y_5_start = y_5_current;

                }

                cmdvelpub.publish(cmdvelmsg);

                break;
            }
            case JedzWzdluz:
            {
                //ROS_INFO("Y: %f", y_5_current);
                if(abs(y_5_current-y_5_start))

                break;
            }
            case ObrotPrawo: break;
        }


    /*        if(i > 150)
            {
                i = 0;

                cmdvelmsg.angular.z = 0.2;
                cmdvelmsg.linear.x = 0.0;
                cmdvelpub.publish(cmdvelmsg);
                sleep(5);
                //msg.points[3].x;

                cmdvelmsg.angular.z = 0.0;
                cmdvelmsg.linear.x = 0.25;
                cmdvelpub.publish(cmdvelmsg);
                sleep(4);

                cmdvelmsg.angular.z = -0.2 * 2;
                cmdvelmsg.linear.x = 0.0;
                cmdvelpub.publish(cmdvelmsg);
                sleep(5);

                cmdvelmsg.angular.z = 0.0;
                cmdvelmsg.linear.x = 0.25;
                cmdvelpub.publish(cmdvelmsg);
                sleep(4);

                cmdvelmsg.angular.z = 0.2;
                cmdvelmsg.linear.x = 0.0;
                cmdvelpub.publish(cmdvelmsg);
                sleep(5);

                cmdvelmsg.angular.z = 0.0;
                cmdvelmsg.linear.x = 0.2;
                cmdvelpub.publish(cmdvelmsg);
                sleep(1);

                cmdvelmsg.angular.z = 0.0;
                cmdvelmsg.linear.x = 0.0;
                cmdvelpub.publish(cmdvelmsg);

                ROS_INFO("OBSTACLE DODGED :3");
                break;
            }
        }


        /*
        if (drive)
        {
          cmdvelpub.publish(cmdvelmsg);	 //  wysyła prędkości x i z do robota
        }

        else
        {
          cmdvelmsg.linear.x  = 0;
          cmdvelmsg.angular.z = 0;
          cmdvelpub.publish(cmdvelmsg);	 //pamiętaj o zatrzymaniu robota!
        }
        */
	
    }
// ----------------- koniec pętli głównej ----------------- 
    
    ROS_INFO("THX GBYE");

    return 0;
}
